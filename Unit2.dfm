object Form2: TForm2
  Left = 0
  Top = 0
  Caption = 'Form2'
  ClientHeight = 218
  ClientWidth = 482
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 24
    Top = 40
    Width = 225
    Height = 137
    Lines.Strings = (
      'Memo1')
    TabOrder = 0
  end
  object Abrir: TButton
    Left = 320
    Top = 120
    Width = 121
    Height = 41
    Caption = 'Abrir'
    TabOrder = 1
    OnClick = AbrirClick
  end
  object MainMenu1: TMainMenu
    Left = 296
    Top = 64
    object Arquivo1: TMenuItem
      Caption = 'Arquivo'
      object Abrir1: TMenuItem
        Caption = 'Abrir'
        OnClick = Abrir1Click
      end
    end
    object Download1: TMenuItem
      Caption = 'Download'
      OnClick = Download1Click
    end
  end
  object Navegador: TNetHTTPClient
    Asynchronous = False
    ConnectionTimeout = 60000
    ResponseTimeout = 60000
    AllowCookies = True
    HandleRedirects = True
    UserAgent = 'Embarcadero URI Client/1.0'
    Left = 400
    Top = 48
  end
end
